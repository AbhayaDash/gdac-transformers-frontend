import os.path
from time import sleep
import pytest
import wget
import tempfile

from ui_tests.test_basic_login import LoggedInTestCase


class TestPreviewSampleDataLink(LoggedInTestCase):
    username = "trial_user"
    password = "123"

    @pytest.mark.ui_test
    def test_preview_sample_data_link(self):
        link_object = self.driver.find_element_by_xpath('/html/body/div[1]/div/div[3]/div/div[2]/div[2]/div[2]/div[1]/a[1]')
        file_url = link_object.get_attribute("href")
        with tempfile.TemporaryDirectory() as temp_dir:
            wget.download(file_url, temp_dir)

            sleep(2)
            self.assertTrue(os.path.isfile(os.path.join(temp_dir, "ConditionMeasurementsSampleData.xlsx")))

        link_object.click()  # Smoke test
