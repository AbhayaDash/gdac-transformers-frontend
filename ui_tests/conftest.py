import testing_utils.conftest as test_config


def pytest_collection_modifyitems(config, items):
    test_config.pytest_collection_modifyitems(config=config, items=items)


def pytest_configure(config):
    test_config.pytest_configure(config=config)


def pytest_addoption(parser):
    test_config.pytest_addoption(parser=parser)
