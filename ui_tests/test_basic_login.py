import os.path
from time import sleep
import pytest

from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from testing_utils.base_selenium_test_case import BaseSeleniumTestCase, SAUCELABS_TESTING_CONFIG


class LoggedInTestCase(BaseSeleniumTestCase):
    """
    Base class to initiate a logged in session and log out when done.

    Subclasses must define a username and password as class attributes.
    """

    testing_config = SAUCELABS_TESTING_CONFIG

    def setUp(self):
        super().setUp()
        self.driver.get(self.test_url + "/login/")  # TODO: Remove login endpoint and click on the login button instead
        self.driver.set_window_size(1920, 1080)
        # self.driver.find_element(By.CSS_SELECTOR, "#menu-item-115 > .nav-link").click()
        self.driver.find_element(By.ID, "username-64").click()
        self.driver.find_element(By.ID, "username-64").send_keys(self.username)
        self.driver.find_element(By.ID, "user_password-64").send_keys(self.password)
        self.driver.find_element(By.ID, "um-submit-btn").click()
        sleep(5)

    def tearDown(self):
        drop_down = self.driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/nav/div/div/ul/li[5]')
        button = drop_down.find_element_by_xpath('ul/li[2]/a')
        builder = ActionChains(self.driver)
        builder.move_to_element(drop_down).click(button).perform()
        super().tearDown()


class TestBasicLogin(LoggedInTestCase):
    username = "trial_user"
    password = "123"

    @pytest.mark.ui_test
    def test_login(self):
        self.assertEqual(self.driver.current_url, self.test_url + "trial/")
