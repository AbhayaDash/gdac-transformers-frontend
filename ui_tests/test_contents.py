import pytest
from testing_utils.base_selenium_test_case import BaseSeleniumTestCase, SAUCELABS_TESTING_CONFIG


@pytest.mark.ui_test
class ContentCheckupTestCase(BaseSeleniumTestCase):
    testing_config = SAUCELABS_TESTING_CONFIG

    def setUp(self):
        super().setUp()

    @pytest.mark.ui_test
    def test_power_rating_text(self):
        self.driver.get(self.test_url)
        self.driver.set_window_size(1920, 1080)

        power_rating_text_element = self.driver.find_element_by_xpath(
            "/html/body/div[1]/div/div[5]/div/div/div[1]/div"
        )

        power_rating_header = power_rating_text_element.find_element_by_tag_name("strong")
        assert "MVA" in power_rating_header.text
        assert "kVA" not in power_rating_header.text
        assert "KVA" not in power_rating_header.text

        power_rating_paragraph = power_rating_text_element.find_element_by_tag_name("p")
        assert "MVA" in power_rating_paragraph.text
        assert "kVA" not in power_rating_paragraph.text
        assert "KVA" not in power_rating_paragraph.text

    @pytest.mark.ui_test
    def test_power_rating_proportions(self):
        self.driver.get(self.test_url)
        self.driver.set_window_size(1920, 1080)

        website_transformer_size_mva_proportions = self.driver.execute_script('return chart_2_data')

        expected_transformer_size_mva_proportions = [
            {'powerRating': 'Under 10', 'number': '18.25'},
            {'powerRating': '10 to 19.9', 'number': '51.15'},
            {'powerRating': '20 to 39.9', 'number': '17.82'},
            {'powerRating': '40 to 59.9', 'number': '4.6'},
            {'powerRating': '60 and above', 'number': '8.19'}
        ]

        self.assertEqual(
            website_transformer_size_mva_proportions,
            expected_transformer_size_mva_proportions,
            msg="The transformer size based on MVA do not match!"
        )
