import pytest

from ui_tests.test_basic_login import LoggedInTestCase


class TestEmailEasyTrial(LoggedInTestCase):
    username = "trial_user"
    password = "123"

    @pytest.mark.ui_test
    def test_check_email_href_elements(self):
        assert self.driver.find_element_by_xpath(
            '/html/body/div[1]/div/div[3]/div/div[2]/div[2]/div[3]/p/a').get_attribute(
            'href') == "mailto:gdac@solvewithvia.com"
