# GDAC Transformers Frontend    



[![Codeship Status for viascience_dev/gdac-transformers-frontend](https://app.codeship.com/projects/7337efb0-9576-0137-4c53-6ec0e672d28e/status?branch=user-management)](https://app.codeship.com/projects/357188)


## WordPress Database Clone
For a new release, we clone the previous version of production WordPress database as a starting point. 
This should be done before merging to master since we need to set the new database host in `infrastructure/env_vars/production.env`. 
In particular, we need to update `DB_HOST` environment variable. 
The database cloning is automated using Terraform. Please note that the latest available snapshot of previous database is cloned.