import sys
import yaml
import logging
import boto3


if __name__ == "__main__":
    print(sys.argv)
    try:
        launch_file_name = sys.argv[1]
        instance_tag_timestamp = sys.argv[2]
        branch_name = sys.argv[3]
    except IndexError:
        logging.error('''
Usage:
python tag_instance_branch.py <launch file path> <instance tag timestamp> <branch_name>        
                ''')
        sys.exit(1)

    try:
        with open(launch_file_name, 'r') as stream:
            launch_settings = yaml.load(stream)
    except FileNotFoundError:
        logging.error("File: {} not found. Please check the specified path.".format(launch_file_name))
        sys.exit(1)

    instance_name = "-".join([launch_settings["ec2"]["Tags"][0]["Value"], instance_tag_timestamp])
    region_name = launch_settings["region"]
    ec2 = boto3.resource('ec2', region_name=region_name)
    instances = ec2.instances.filter(
        Filters=[{'Name': 'tag:Name', 'Values': [instance_name]},
                 {'Name': 'instance-state-name', 'Values': ['running']}])

    for instance in instances:
        instance.create_tags(DryRun=False,
                             Tags=[
                                 {
                                    'Key': 'Branch',
                                    'Value': f"{branch_name}"
                                 },
                             ])