#!/usr/bin/env bash
export PYTHONUNBUFFERED=1
stdbuf -oL -eL ansible-playbook "$@" 1>>output.log 2>>error.log &
PID1=$!
stdbuf -oL -eL tail -F error.log &
stdbuf -oL -eL tail -F output.log &
wait $PID1
returnValue=$?
sleep 2s
if [ $returnValue -ne 0 ]; then
    echo "rt_ansible.sh exit code: $returnValue"
fi
rm error.log
rm output.log
exit $returnValue