PLEASE DO NOT PUT ANY SECRET IN THIS DIRECTORY.
The environment variables defined here are under git revision and they are not secrets, hence no encryption is needed!
If you want to add a secret, please either use codeship secrets or call secrets from AWS secret manager.
