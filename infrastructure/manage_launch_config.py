import sys
import yaml
import logging

DEV_DEPLOY_MODES = {
    "staging": "Staging",
    "dev": "Dev",
    "feature": "Feature",
}

if __name__ == "__main__":
    try:
        launch_file_name = sys.argv[1]
        deployment_mode = sys.argv[2]
    except IndexError:
        logging.error('''
        Usage:
        python manage_launch_config.py <launch file path> <dev|staging|production>         
                ''')
        sys.exit(1)
    try:
        with open(launch_file_name, 'r') as stream:
            launch_settings = yaml.load(stream)
    except FileNotFoundError:
        logging.error("ERROR!! File: {} not found. Please check the specified path.".format(launch_file_name))
        sys.exit(1)
    instance_name = launch_settings["ec2"]["Tags"][0]["Value"]

    if deployment_mode in DEV_DEPLOY_MODES.keys():
        logging.info("Received Command: {} as Deployment Mode".format(deployment_mode))
        launch_settings["ec2"]["Tags"][0]["Value"] = instance_name.replace("-mode", DEV_DEPLOY_MODES[deployment_mode])
        launch_settings["ec2"]["System"]["KeyName"] = "dev_central_10202020"
        launch_settings["ec2"]["System"]["SubnetId"] = "subnet-05ff1a70569b04915"
        launch_settings["ec2"]["System"]["SecurityGroupIds"][0] = "sg-014221d25abd41b26"
        if deployment_mode == "feature":
            launch_settings["ec2"]["ElasticIPs"][0]["Value"] = "No"
    elif deployment_mode == "production":
        logging.info("Received Command: {} as Deployment Mode".format(deployment_mode))
        launch_settings["ec2"]["Tags"][0]["Value"] = instance_name.replace("-mode", "Production")
        launch_settings["ec2"]["System"]["KeyName"] = "production_central_10202020"
        launch_settings["ec2"]["System"]["SubnetId"] = "subnet-0bb443c06c807bd92"
        launch_settings["ec2"]["System"]["SecurityGroupIds"][0] = "sg-01adac9ec06485035"
    else:
        logging.error("Incorrect option supplied!! Check again!")
        sys.exit(1)

    print("Changed Launch Config...")
    new_launch_settings = yaml.dump(launch_settings, default_flow_style=False)
    with open(launch_file_name, "w") as w:
        w.write(new_launch_settings)
