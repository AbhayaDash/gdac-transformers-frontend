import logging
import requests
import yaml
import sys

if __name__ == "__main__":
    print(sys.argv)
    try:
        launch_file_name = sys.argv[1]
    except IndexError:
        logging.error('''
        Usage:
        python banner.py <tag> <launch file path> 
        ''')
        sys.exit(1)
    try:
        with open(launch_file_name, 'r') as stream:
            launch_settings = yaml.load(stream)
    except FileNotFoundError:
        logging.error("ERROR!! File: {} not found. Please check the specified path.".format(launch_file_name))
        sys.exit(1)
    instance_name = launch_settings["ec2"]["Tags"][0]["Value"]
    banner_content = str()
    try:
        res = requests.get("http://artii.herokuapp.com/make?text={}".format(instance_name))
        banner_content = res.content
    except:
        banner_content = "\n".join(["#"*100, instance_name, "#"*100])
        banner_content = str.encode(banner_content)
    with open('banner.art', 'wb') as w:
        w.write(banner_content)
        w.write(b"\n")
